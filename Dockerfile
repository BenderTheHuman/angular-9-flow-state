FROM node:10-alpine
WORKDIR /app
COPY . /app
RUN npm install
RUN npm install -g @angular/cli
RUN npm ng build --prod

# Expose the port the app runs in
EXPOSE 4200

# Serve the app
CMD ["npm", "start"]
