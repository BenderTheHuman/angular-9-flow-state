/***************************************************************************************************
 * Load `$localize` onto the global scope - used if i18n tags appear in Angular templates.
 */
import '@angular/localize/init';
import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';

import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';

// The Express app is exported so that it can be used by serverless Functions.
export function app() {

  if (process.env.NODE_ENV !== 'production') {
    require('dotenv').load();
  }

  const stripeSecretKey =  process.env.STRIPE_PUBLIC_KEY;
  const stripePublicKey =  process.env.STRIPE_PRIVATE_KEY;

  const server = express();

  const hsts = require('hsts');

  server.use(hsts({
    maxAge: 15552000  // 180 days in seconds
  }));

  server.use((req, res, next) => {
    if (req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === 'http') {
      return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    next();
  });

  const distFolder = join(process.cwd(), 'dist/flow-state-serverside/browser');
  const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

  // Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
  server.engine('html', ngExpressEngine({
    bootstrap: AppServerModule,
  }));

  server.set('view engine', 'html');
  server.set('views', distFolder);

  server.get('/sitemap.xml', (req, res) => {
    return res.sendFile(distFolder + '/assets/' + 'sitemap.xml');
  });

  server.get('/robots.txt', (req, res) => {
    return res.sendFile(distFolder + '/assets/' + 'robots.txt');
  });

  // Example Express Rest API endpoints
  // app.get('/api/**', (req, res) => { });

  // Set your secret key: remember to switch to your live secret key in production
  // See your keys here: https://dashboard.stripe.com/account/apikeys
  // const stripe = require('stripe')(stripeSecretKey);

  // server.post('/api/create-checkout', async (req, res) => {

  //   console.log(req.body);

  //   await stripe.checkout.sessions.create({
  //     payment_method_types: ['card'],
  //     line_items: [{
  //       name: 'T-shirt',
  //       description: 'Comfortable cotton t-shirt',
  //       images: ['https://example.com/t-shirt.png'],
  //       amount: 500,
  //       currency: 'usd',
  //       quantity: 1,
  //     }],
  //     success_url: 'https://flowstatefly.com/shop/checkout-process/success?session_id={CHECKOUT_SESSION_ID}',
  //     cancel_url: 'https://flowstatefly.com/shop/checkout',
  //   });
  // });

  // Serve static files from /browser
  server.get('*.*', express.static(distFolder, {
    maxAge: '1y'
  }));

  // All regular routes use the Universal engine
  server.get('*', (req, res) => {
    res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
  });

  return server;
}

function run() {
  const port = process.env.PORT || 4000;

  // Start up the Node server
  const server = app();
  server.listen(port, () => {
    console.log(`Node Express server listening on http://localhost:${port}`);
  });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = mainModule && mainModule.filename || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
  run();
}

export * from './src/main.server';
