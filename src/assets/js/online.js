export async function checkInternet() {
  let ping = require('ping');

  let host = 'google.com';

  await ping.promise.probe(host)
    .then(function (res) {
      return res.alive;
  }).catch(e => console.log(e));

}
