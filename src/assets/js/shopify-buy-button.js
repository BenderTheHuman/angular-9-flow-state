/*<![CDATA[*/
(function () {
  var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
  if (window.ShopifyBuy) {
    if (window.ShopifyBuy.UI) {
      ShopifyBuyInit();
    } else {
      loadScript();
    }
  } else {
    loadScript();
  }
  function loadScript() {
    var script = document.createElement('script');
    script.async = true;
    script.src = scriptURL;
    script.id = 'buy-button-mini';
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
    script.onload = ShopifyBuyInit;
  }
  function ShopifyBuyInit() {
    var client = ShopifyBuy.buildClient({
      domain: 'trying-987987987.myshopify.com',
      storefrontAccessToken: '3c9237ed92a22cf924ecaeaa401696a3',
    });
    ShopifyBuy.UI.onReady(client).then(function (ui) {
      ui.createComponent('collection', {
        id: '152822218823',
        node: document.getElementById('collection-component-1571443572829'),
        moneyFormat: '%24%7B%7Bamount%7D%7D',
        options: {
  "product": {
    "styles": {
      "product": {
        "@media (min-width: 601px)": {
          "max-width": "calc(25% - 20px)",
          "margin-left": "20px",
          "margin-bottom": "50px",
          "width": "calc(25% - 20px)"
        },
        "img": {
          "height": "calc(100% - 15px)",
          "position": "absolute",
          "left": "0",
          "right": "0",
          "top": "0"
        },
        "imgWrapper": {
          "padding-top": "calc(75% + 15px)",
          "position": "relative",
          "height": "0"
        }
      },
      "button": {
        "font-family": "Garamond, serif",
        "font-size": "17px",
        "padding-top": "16.5px",
        "padding-bottom": "16.5px",
        "color": "#e6921d",
        ":hover": {
          "color": "#e6921d",
          "background-color": "#0f4c26"
        },
        "background-color": "#11542a",
        ":focus": {
          "background-color": "#0f4c26"
        },
        "border-radius": "40px",
        "padding-left": "29px",
        "padding-right": "29px"
      },
      "quantityInput": {
        "font-size": "17px",
        "padding-top": "16.5px",
        "padding-bottom": "16.5px"
      }
    },
    "buttonDestination": "modal",
    "contents": {
      "options": false
    },
    "text": {
      "button": "View product"
    }
  },
  "productSet": {
    "styles": {
      "products": {
        "@media (min-width: 601px)": {
          "margin-left": "-20px"
        }
      }
    }
  },
  "modalProduct": {
    "contents": {
      "img": false,
      "imgWithCarousel": true,
      "button": false,
      "buttonWithQuantity": true
    },
    "styles": {
      "product": {
        "@media (min-width: 601px)": {
          "max-width": "100%",
          "margin-left": "0px",
          "margin-bottom": "0px"
        }
      },
      "button": {
        "font-family": "Garamond, serif",
        "font-size": "17px",
        "padding-top": "16.5px",
        "padding-bottom": "16.5px",
        "color": "#e6921d",
        ":hover": {
          "color": "#e6921d",
          "background-color": "#0f4c26"
        },
        "background-color": "#11542a",
        ":focus": {
          "background-color": "#0f4c26"
        },
        "border-radius": "40px",
        "padding-left": "29px",
        "padding-right": "29px"
      },
      "quantityInput": {
        "font-size": "17px",
        "padding-top": "16.5px",
        "padding-bottom": "16.5px"
      }
    },
    "text": {
      "button": "Add to cart"
    }
  },
  "cart": {
    "styles": {
      "button": {
        "font-family": "Garamond, serif",
        "font-size": "17px",
        "padding-top": "16.5px",
        "padding-bottom": "16.5px",
        "color": "#e6921d",
        ":hover": {
          "color": "#e6921d",
          "background-color": "#0f4c26"
        },
        "background-color": "#11542a",
        ":focus": {
          "background-color": "#0f4c26"
        },
        "border-radius": "40px"
      }
    },
    "text": {
      "total": "Subtotal",
      "button": "Checkout"
    }
  },
  "toggle": {
    "styles": {
      "toggle": {
        "font-family": "Garamond, serif",
        "background-color": "#11542a",
        ":hover": {
          "background-color": "#0f4c26"
        },
        ":focus": {
          "background-color": "#0f4c26"
        }
      },
      "count": {
        "font-size": "17px",
        "color": "#e6921d",
        ":hover": {
          "color": "#e6921d"
        }
      },
      "iconPath": {
        "fill": "#e6921d"
      }
    }
  }
},
      });
    });
  }
})();
/*]]>*/
