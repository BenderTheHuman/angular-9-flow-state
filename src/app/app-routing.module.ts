import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'shop',
      loadChildren: () => import('./web-store/web-store.module').then(m => m.WebStoreModule)
  },
  { path: 'stream',
      loadChildren: () => import('./streaming/streaming.module').then(m => m.StreamModule)
  },
  {
    path: '',
    redirectTo: '/home', pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/home', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
