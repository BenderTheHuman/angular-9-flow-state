import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { WebStoreRoutingModule } from './web-store-routing.module';
import { StoreFrontComponent } from './store-front/store-front.component';

import { StoreHomeCardsComponent } from './shop-home/store-home-cards/store-home-cards.component';
import { ShopCartComponent } from './shop-cart/shop-cart.component';
import { ShopHomeComponent } from './shop-home/shop-home.component';
import { ProductsModule } from './shop-products/products-module/products.module';

import { ShopCartService } from '../shared/services/shop-cart.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    StoreFrontComponent,
    StoreHomeCardsComponent,
    ShopCartComponent,
    ShopHomeComponent
  ],
  providers: [
    ShopCartService
  ],
  imports: [
    CommonModule,
    WebStoreRoutingModule,
    ProductsModule,
    FormsModule
  ]
})
export class WebStoreModule { }
