import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { transAnimationRight, transAnimationLeft, transAnimationUp } from 'src/app/shared/animations/route-animations';
import { RouterOutlet, Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shop-products',
  templateUrl: './shop-products.component.html',
  styleUrls: ['./shop-products.component.sass'],
  animations: [
    trigger('storeProductsRouteAnimations', [
      transition('productDescription => *', [
        useAnimation(transAnimationUp)
      ]),
      transition('* => productsAllPage', [
        useAnimation(transAnimationLeft)
      ]),
      transition('* => accessoriesPage', [
        useAnimation(transAnimationRight)
      ]),
      transition('* => jerseysPage', [
        useAnimation(transAnimationLeft)
      ]),
      transition('* => shortSleevesPage', [
        useAnimation(transAnimationRight)
      ]),
      transition('* => productDescription', [
        useAnimation(transAnimationUp)
      ])
    ])
  ]
})
export class ShopProductsComponent implements OnInit, OnDestroy {

  private reroute;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private zone: NgZone
  ) {

    this.reroute = router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        if (e.url === '/shop/products') {
          this.zone.run(() => {
            this.router.navigate(['/shop/products/all']);
          });
        }
      }
    });
   }

  ngOnInit(): void {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  ngOnDestroy() {
    this.reroute.unsubscribe();
  }

}
