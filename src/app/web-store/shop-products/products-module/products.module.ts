import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopProductsComponent } from '../shop-products.component';

import { ProductsRoutingModule } from './products-routing.module';
import { AccessoriesComponent } from './accessories/accessories.component';
import { JerseysComponent } from './jerseys/jerseys.component';
import { ShortSleevesComponent } from './short-sleeves/short-sleeves.component';
import { ProductsService } from '../../../shared/services/products.service';
import { AllProductsComponent } from './all-products/all-products.component';
import { ItemCardTemplateComponent } from './item-card-template/item-card-template.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ShopProductsComponent,
    AccessoriesComponent,
    JerseysComponent,
    ShortSleevesComponent,
    AllProductsComponent,
    ItemCardTemplateComponent,
    ProductDescriptionComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule
  ],
  providers : [
    ProductsService
  ]
})
export class ProductsModule { }
