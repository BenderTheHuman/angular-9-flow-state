import { Component, OnInit } from '@angular/core';

import { ProductsService } from '../../../../shared/services/products.service';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.sass']
})
export class AllProductsComponent implements OnInit {

  public productData;
  public products = [];

  constructor(private productService: ProductsService) { }

  ngOnInit(): void {
    this.productData = this.productService.getProducts()[0];
    Object.keys(this.productData).forEach((item) => {
      this.productData[item].forEach((el) => {
        this.products.push(el);
      });
    });
    this.products.push('all products');
  }

}
