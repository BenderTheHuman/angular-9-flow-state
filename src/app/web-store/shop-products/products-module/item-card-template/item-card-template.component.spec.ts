import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCardTemplateComponent } from './item-card-template.component';

describe('ItemCardTemplateComponent', () => {
  let component: ItemCardTemplateComponent;
  let fixture: ComponentFixture<ItemCardTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemCardTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCardTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
