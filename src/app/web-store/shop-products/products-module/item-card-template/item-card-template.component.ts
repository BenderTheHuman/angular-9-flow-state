import { Component, OnInit, Input, Inject } from '@angular/core';
import { ShopCartService } from 'src/app/shared/services/shop-cart.service';
import { WINDOW } from '@ng-toolkit/universal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-card-template',
  templateUrl: './item-card-template.component.html',
  styleUrls: ['./item-card-template.component.sass']
})
export class ItemCardTemplateComponent implements OnInit {

  @Input() products;
  public productTitle;

  constructor(
    private shoppingCartService: ShopCartService,
    @Inject(WINDOW) private window: Window,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.productTitle = this.products.pop();
  }

  addToCart(id) {
    this.shoppingCartService.addToCart(id, 1);
  }

  public goToPage(productId, event) {
    this.router.navigate(['/', 'shop', 'products', productId]);
    event.stopPropagation();
    this.window.scrollTo(0, 300);
  }

}
