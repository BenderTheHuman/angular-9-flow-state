import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccessoriesComponent } from './accessories/accessories.component';
import { AllProductsComponent } from './all-products/all-products.component';
import { ShopProductsComponent } from '../shop-products.component';
import { JerseysComponent } from './jerseys/jerseys.component';
import { ShortSleevesComponent } from './short-sleeves/short-sleeves.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';


const routes: Routes = [
    {
        path: '',
        component: ShopProductsComponent,
        data: {animation: 'productsTopPage'}, // products template component
        children: [
          {
            path: 'all',
            component: AllProductsComponent,
            data: {animation: 'productsAllPage'}
          },
          {
            path: 'accessories',
            component: AccessoriesComponent,
            data: {animation: 'accessoriesPage'}
          },
          {
            path: 'jerseys',
            component: JerseysComponent,
            data: {animation: 'jerseysPage'}
          },
          {
            path: 'short-sleeves',
            component: ShortSleevesComponent,
            data: {animation: 'shortSleevesPage'}
          },
          {
            path: ':id',
            component: ProductDescriptionComponent,
            data: {animation: 'productDescription'}
          }
        ]
    }
  // {
  //   path: '**',
  //   conponent : PageNotFoundComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class ProductsRoutingModule { }
