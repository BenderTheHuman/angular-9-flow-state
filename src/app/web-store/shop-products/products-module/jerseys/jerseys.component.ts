import { Component, OnInit } from '@angular/core';

import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-jerseys',
  templateUrl: './jerseys.component.html',
  styleUrls: ['./jerseys.component.sass']
})
export class JerseysComponent implements OnInit {

  constructor(private productService: ProductsService) { }

  public jerseys;

  ngOnInit(): void {

    this.jerseys = this.productService.getJerseys();
    this.jerseys.push('jerseys');
  }

}
