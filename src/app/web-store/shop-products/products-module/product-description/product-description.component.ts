import { Component, OnInit, NgZone, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ProductsService } from 'src/app/shared/services/products.service';
import { Title, Meta } from '@angular/platform-browser';
import { ShopCartService } from 'src/app/shared/services/shop-cart.service';

@Component({
  selector: 'app-product-description',
  templateUrl: './product-description.component.html',
  styleUrls: ['./product-description.component.sass']
})
export class ProductDescriptionComponent implements OnInit {

  public productId: string;
  public product;
  public reroute;
  public quantity: number = 1;

  constructor(
    private actRoute: ActivatedRoute,
    private cartService: ShopCartService,
    private productService: ProductsService,
    private route: ActivatedRoute,
    private zone: NgZone,
    private router: Router,
    private title: Title,
    private meta: Meta,
    @Inject(Location) private location: Location) {

      this.reroute = router.events.subscribe(e => {
        if (e instanceof NavigationEnd) {
          this.zone.run(() => {
            this.router.navigate(['/shop/products', this.route.snapshot.paramMap.get('id')]);
          });
        }
      });
   }

  ngOnInit(): void {
    this.productId = this.actRoute.snapshot.params.id;
    this.product = this.productService.find(this.productId);
    console.log(this.product);
    this.reroute.unsubscribe();

    // SEO metadata
    this.title.setTitle(`Flow State ${this.product.name}'s Store Product Detail Page`);
    this.meta.addTag({name: 'description', content: `Flow State ${this.product.name}'s Store Product Detail Page`});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: `https://www.flowstate.com/shop/products/${this.product.id}`});
    this.meta.addTag({name: 'twitter:title', content: `Flow State ${this.product.name}\'s Store Product Detail Page`});
    this.meta.addTag({name: 'twitter:description', content: `Flow State ${this.product.name}\'s Store Product Detail Page`});
    this.meta.addTag({name: 'twitter:text:description', content: `Flow State ${this.product.name}\'s Store Product Detail Page`});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: `https://www.flowstate.com/shop/products/${this.product.id}`});
    this.meta.addTag({name: 'og:title', content: `Flow State ${this.product.name}\'s Store Product Detail Page`});
    this.meta.addTag({name: 'og:description', content: `Flow State ${this.product.name}\'s Store Product Detail Page`});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

  addToCart(productId, quantity) {
    this.cartService.addToCart(productId, quantity);
  }

  goBackToPreviousPage() {
    this.location.back();
  }
}
