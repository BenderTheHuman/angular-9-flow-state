import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortSleevesComponent } from './short-sleeves.component';

describe('ShortSleevesComponent', () => {
  let component: ShortSleevesComponent;
  let fixture: ComponentFixture<ShortSleevesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortSleevesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortSleevesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
