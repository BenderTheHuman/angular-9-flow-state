import { Component, OnInit } from '@angular/core';

import { ProductsService } from 'src/app/shared/services/products.service';

@Component({
  selector: 'app-short-sleeves',
  templateUrl: './short-sleeves.component.html',
  styleUrls: ['./short-sleeves.component.sass']
})
export class ShortSleevesComponent implements OnInit {

  constructor(private productService: ProductsService) { }

  public shortSleeves;

  ngOnInit(): void {

    this.shortSleeves = this.productService.getShortSleeves();
    this.shortSleeves.push('short Sleeves');
  }

}
