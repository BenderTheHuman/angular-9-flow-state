import { Component, OnInit } from '@angular/core';

import { ProductsService } from '../../../../shared/services/products.service';

@Component({
  selector: 'app-accessories',
  templateUrl: './accessories.component.html',
  styleUrls: ['./accessories.component.sass']
})
export class AccessoriesComponent implements OnInit {

  public accessories;

  constructor(private productService: ProductsService) { }

  ngOnInit(): void {
    this.accessories = this.productService.getAccessories();
    this.accessories.push('accessories');
  }

}
