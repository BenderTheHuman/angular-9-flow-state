import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-shop-home',
  templateUrl: './shop-home.component.html',
  styleUrls: ['./shop-home.component.sass']
})
export class ShopHomeComponent implements OnInit {

  constructor(
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {

    // SEO metadata
    this.title.setTitle('Flow State Store');
    this.meta.addTag({name: 'description', content: 'The flow state\'s online store. Be like our Flow State Atheletes'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/shop'});
    this.meta.addTag({name: 'og:title', content: 'Flow State E-commerce Store'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s e-commerce store. Be like our Flow State Atheletes'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/shop'});
    this.meta.addTag({name: 'twitter:title', content: 'Flow State E-commerce Store'});
    this.meta.addTag({name: 'twitter:description', content: 'The flow state\'s e-commerce store. Be like our Flow State Atheletes'});
    this.meta.addTag({name: 'twitter:text:description', content: 'The flow state\'s e-commerce store. Be like our Flow State Atheletes'});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

}
