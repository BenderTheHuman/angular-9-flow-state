import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { Router } from '@angular/router';

import * as data from '../../../../assets/data/top-products.json';

import { WINDOW } from '@ng-toolkit/universal';
import { state, transition, trigger, style, animate } from '@angular/animations';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-store-home-cards',
  templateUrl: './store-home-cards.component.html',
  animations: [
    trigger('onScrollIntoView', [
      state('false', style({opacity: 0, transform: 'translateY(-100px)'})),
      state('true', style({ opacity: 1, transform: 'none' })),
      transition('false => true', animate(1000))
    ])
  ],
  styleUrls: ['./store-home-cards.component.sass']
})
export class StoreHomeCardsComponent implements OnInit {

  public topProducts: any = (data as any).default;
  public cardsInView = false;

  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(DOCUMENT) private document: Document,
    private router: Router
  ) {}

  @HostListener('window:scroll', [])
    onScroll(): void {
      this.areCardsInView();
  }

  ngOnInit() {
  }

  areCardsInView() {
    const homeCards = this.document.querySelector('.top-products');
    const cardBounding = homeCards.getBoundingClientRect();
    if (
      cardBounding.top >= 0 &&
      cardBounding.left >= 0 &&
      cardBounding.right <= (this.window.innerWidth || this.document.documentElement.clientWidth) &&
      cardBounding.bottom <= (this.window.innerHeight || this.document.documentElement.clientHeight)
    ) {
      this.cardsInView = true;
    }
  }

  public goToPage(product, event) {
    this.router.navigate(['/', 'shop', 'products', product]);
    event.stopPropagation();
    this.window.scrollTo(0, 0);
  }
}
