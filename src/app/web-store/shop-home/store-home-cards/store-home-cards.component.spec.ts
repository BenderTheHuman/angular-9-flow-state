import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreHomeCardsComponent } from './store-home-cards.component';

describe('StoreHomeCardsComponent', () => {
  let component: StoreHomeCardsComponent;
  let fixture: ComponentFixture<StoreHomeCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreHomeCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreHomeCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
