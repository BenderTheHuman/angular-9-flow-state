import { Component, OnInit, OnDestroy, Inject, NgZone, HostListener } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, NavigationEnd, RouterOutlet } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { ScriptService } from '../../shared/services/script.service';
import { ShopCartService } from 'src/app/shared/services/shop-cart.service';
import { Subscription } from 'rxjs';
import { trigger, useAnimation, transition } from '@angular/animations';
import { transAnimationLeft, transAnimationRight } from 'src/app/shared/animations/route-animations';
import { WINDOW } from '@ng-toolkit/universal';

@Component({
  selector: 'app-store-front',
  templateUrl: './store-front.component.html',
  styleUrls: ['./store-front.component.sass'],
  animations: [
    trigger('storeRouteAnimations', [
      transition('* => flowShopHome', [
        useAnimation(transAnimationLeft)
      ]),
      transition('* => cart', [
        useAnimation(transAnimationRight)
      ]),
      transition('* => productsTopPage', [
        useAnimation(transAnimationLeft)
      ])
    ])
  ]
})
export class StoreFrontComponent implements OnInit, OnDestroy {

  public reroute;
  public cartQuantity;
  public cartInView = true;
  public cartView = false;
  public newItemAdded = false;
  sub: Subscription;

  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(DOCUMENT) private doc: any,
    private script: ScriptService,
    private shoppingCartService: ShopCartService,
    private zone: NgZone,
    private router: Router,
    private title: Title,
    private meta: Meta
  ) {
    this.reroute = router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        if (e.url === '/shop/cart') {
          this.cartView = true;
        } else {
          this.cartView = false;
        }
        if (e.url === '/shop') {
          this.zone.run(() => {
            this.router.navigate(['/shop']);
          });
        }
      }
    });

    this.sub = this.shoppingCartService.didCartQuantityIncrease().subscribe(didUpdate => {
      if (didUpdate === true) {
        this.ngOnInit();
        this.onAddedToCart();
      } else {
        return;
      }
    });
  }

  @HostListener('window:scroll', [])
  onScroll(): void {
    this.isCartButtonInView();
  }

  ngOnInit() {

    this.loadStripe();
    this.cartQuantity = this.shoppingCartService.cartQuantity();
    // this.script.load('').then(data => {
    //     console.log('script loaded ', data);
    // }).catch(error => console.log(error));

    // SEO metadata
    this.title.setTitle('Flow State Online Store');
    this.meta.addTag({ name: 'description', content: 'The flow state\'s online store. See what we are selling.' });

    // Twitter metadata
    this.meta.addTag({ name: 'twitter:card', content: 'summary' });
    this.meta.addTag({ name: 'twitter:site', content: 'https://www.flowstate.com/shop' });
    this.meta.addTag({ name: 'twitter:title', content: 'Flow State Online Store' });
    this.meta.addTag({ name: 'twitter:description', content: 'The flow state\'s online store. See what we are selling.' });
    this.meta.addTag({ name: 'twitter:text:description', content: 'The flow state\'s online store. See what we are selling.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://www.flowstate.com/assets/images/logoCircles.png' });
  }

  isCartButtonInView() {
    if (this.cartView === false) {
      const homeCards = this.doc.querySelector('.cart-button');
      const cardBounding = homeCards.getBoundingClientRect();
      if (this.cartInView !== false) {
        if (
          cardBounding.top <= (this.window.innerHeight || this.doc.documentElement.clientHeight) &&
          cardBounding.left >= 0 &&
          cardBounding.right >= (this.window.innerWidth || this.doc.documentElement.clientWidth) &&
          cardBounding.bottom >= 0
        ) {
        } else {
          this.cartInView = false;
        }
      }
    }
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  private removeScript(id: string) {
    if (this.doc.getElementById(id) !== null) {
      const script = this.doc.getElementById(id);
      script.parentNode.removeChild(script);
    }

  }

  onAddedToCart() {
    this.newItemAdded = true;
    setTimeout(() => {
      this.newItemAdded = false;
    }, 200);
  }

  goToPage(cart, event) {
    this.router.navigate(['/', 'shop', cart]);
    event.stopPropagation();
    this.window.scrollTo(0, 100);
  }

  loadStripe() {
    if (!this.window.document.getElementById('stripe-script')) {
      const s = this.window.document.createElement('script');
      s.id = 'stripe-script';
      s.type = 'text/javascript';
      s.src = 'https://checkout.stripe.com/checkout.js';
      this.window.document.body.appendChild(s);
    }
}

  ngOnDestroy() {
    this.removeScript('stripe-script');
    console.log('removed stripe script');
    this.sub.unsubscribe();
    this.reroute.unsubscribe();
  }

}
