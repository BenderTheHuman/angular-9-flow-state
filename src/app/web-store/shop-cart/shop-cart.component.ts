import { Component, OnInit, NgZone } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

import { ShopCartService } from 'src/app/shared/services/shop-cart.service';
import { NavigationEnd, Router, ActivatedRoute } from '@angular/router';

declare var Stripe: any;

@Component({
  selector: 'app-shop-cart',
  templateUrl: './shop-cart.component.html',
  styleUrls: ['./shop-cart.component.sass']
})
export class ShopCartComponent implements OnInit {

  public cart;
  public totalCost;
  public reroute;
  public stripe;
  public skus;

  constructor(
    private shoppingCartService: ShopCartService,
    private route: ActivatedRoute,
    private zone: NgZone,
    private router: Router,
    private title: Title,
    private meta: Meta,
  ) {
    this.reroute = router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        if (e.url === '/shop/cart') {
          this.zone.run(() => {
            this.router.navigate(['/shop/cart']);
          });
        }
      }
    });
   }

  ngOnInit() {

    this.reroute.unsubscribe();

    this.stripe = Stripe('pk_test_GDHIQpfDKn5qoENjcCtnt92v00poW4dmDV');

    this.cart = this.shoppingCartService.loadCart();
    if (this.cart !== null) {
      console.log(this.cart);
      this.totalCost = this.cart.pop();
    }
    // SEO metadata
    this.title.setTitle('Flow State Store Cart');
    this.meta.addTag({name: 'description', content: 'The flow state\'s online store cart. See the things you want to buy and checkout'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/shop/cart'});
    this.meta.addTag({name: 'og:title', content: 'Flow State E-commerce Store Cart'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s online store cart. See the things you want to buy and checkout'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/shop/cart'});
    this.meta.addTag({name: 'twitter:title', content: 'Flow State E-commerce Store Cart'});
    this.meta.addTag(
      {
        name: 'twitter:description',
        content: 'The flow state\'s online store cart. See the things you want to buy and checkout'
      }
    );
    this.meta.addTag(
      {
        name: 'twitter:text:description',
        content: 'The flow state\'s online store cart. See the things you want to buy and checkout'
      }
    );
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

  removeItem(id) {
    this.shoppingCartService.remove(id);
    this.ngOnInit();
  }

  editItem(id, quantity) {
    this.shoppingCartService.editItem(id, quantity);
    this.ngOnInit();
  }

  goToCheckout() {
    this.skus = this.shoppingCartService.loadSkus();
    console.log(this.skus);
    this.stripe.redirectToCheckout({
      items: this.skus,
      billingAddressCollection: 'required',
      successUrl: 'http://localhost:4200/shop/cart?success',
      cancelUrl: 'http://localhost:4200/shop/cart?failure',
    }).then((result) => {
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
    });
  }

}
