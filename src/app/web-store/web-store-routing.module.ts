import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StoreFrontComponent } from './store-front/store-front.component';
import { ShopCartComponent } from './shop-cart/shop-cart.component';
import { ShopHomeComponent } from './shop-home/shop-home.component';


const routes: Routes = [
  {
    path: '',
    component: StoreFrontComponent,
    data: {animation: 'flowStore'}, // base template component
    children: [
      {
        path: '',
        component: ShopHomeComponent,
        data: {animation: 'flowShopHome'}
      },
      {
        path: 'products',
        loadChildren: () => import('./shop-products/products-module/products.module').then(m => m.ProductsModule)
      },
      {
        path: 'cart',
        component: ShopCartComponent,
        data: {animation: 'cart'}
      }
    ]
  }
  // {
  //   path: '**',
  //   conponent : PageNotFoundComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class WebStoreRoutingModule { }
