import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NgtUniversalModule } from '@ng-toolkit/universal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FlowAppRoutingModule } from './flow-pages/flow-app-routing.module';

import { FlowAppModule } from './flow-pages/flow-app.module';
import { WebStoreModule } from './web-store/web-store.module';

import { NavBarComponent } from './nav-bar/nav-bar.component';

import { ScriptService } from './shared/services/script.service';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StreamModule } from './streaming/streaming.module';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'flow-state-serverside' }),
    RouterModule,
    BrowserAnimationsModule,
    FlowAppRoutingModule,
    AppRoutingModule,
    CommonModule,
    FlowAppModule,
    HttpClientModule,
    NgtUniversalModule,
    NgbModule,
    WebStoreModule,
    StreamModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  exports: [NgbModule],
  providers: [
    ScriptService,
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
