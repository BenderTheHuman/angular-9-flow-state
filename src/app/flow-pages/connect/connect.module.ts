import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ConnectComponent } from './connect.component';

import { ConnectRoutingModule } from './connect-routing.module';


@NgModule({
  declarations: [
    ConnectComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    ConnectRoutingModule
  ]
})
export class ConnectModule { }
