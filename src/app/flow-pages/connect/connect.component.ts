import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.sass']
})
export class ConnectComponent implements OnInit {

  constructor(
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {

    // SEO metadata
    this.title.setTitle('Flow State Connect Page');
    this.meta.addTag({name: 'description', content: 'The flow state\'s connection page. Contact us here.'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/connect'});
    this.meta.addTag({name: 'og:title', content: 'Flow State Connect Page'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s connection page. Contact us here.'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/connect'});
    this.meta.addTag({name: 'twitter:title', content: 'Flow State Connect Page'});
    this.meta.addTag({name: 'twitter:description', content: 'The flow state\'s connection page. Contact us here.'});
    this.meta.addTag({name: 'twitter:text:description', content: 'The flow state\'s connection page. Contact us here.'});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

}
