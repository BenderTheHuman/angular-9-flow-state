import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./landing-page/landing-page.module').then(m => m.LandingPageModule)
  },
  {
    path: 'our-story',
    loadChildren: () => import('./our-story/our-story.module').then(m => m.OurStoryModule)
  },
  {
    path: 'athletes',
    loadChildren: () => import('./athletes/athletes.module').then(m => m.AthletesModule)
  },
  {
    path: 'media',
    loadChildren: () => import('./media/media.module').then(m => m.MediaModule)
  },
  {
    path: 'services',
    loadChildren: () => import('./price-page/price-page.module').then(m => m.PricePageModule)
  },
  {
    path: 'calender',
    loadChildren: () => import('./calender/calender.module').then(m => m.CalenderModule)
  },
  {
    path: 'connect',
    loadChildren: () => import('./connect/connect.module').then(m => m.ConnectModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlowAppRoutingModule { }
