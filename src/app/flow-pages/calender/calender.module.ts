import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CalenderComponent } from './calender.component';

import { CalenderRoutingModule } from './calender-routing.module';


@NgModule({
  declarations: [
    CalenderComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    CalenderRoutingModule
  ]
})
export class CalenderModule { }
