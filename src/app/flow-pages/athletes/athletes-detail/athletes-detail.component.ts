import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { AthleteService } from '../../../shared/services/athlete-service';

@Component({
  selector: 'app-athletes-detail',
  templateUrl: './athletes-detail.component.html',
  styleUrls: ['./athletes-detail.component.sass']
})
export class AthletesDetailComponent implements OnInit {
  public athlete;
  public id: number;
  public reroute;

  constructor(
    private route: ActivatedRoute,
    public athleteService: AthleteService,
    private zone: NgZone,
    private router: Router,
    private title: Title,
    private meta: Meta
  ) {

    this.reroute = router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.zone.run(() => {
          this.router.navigate(['/athletes', +this.route.snapshot.paramMap.get('id')]);
        });
      }
    });

  }

  ngOnInit() {

    this.id = +this.route.snapshot.paramMap.get('id');
    const data = this.athleteService.getAthlete(this.id)[0];
    this.athlete = {
      name: data.name,
      id: data.id,
      introduction: data.introduction,
      imageUrl: data.imageUrl,
      instagramUrl: data.instagramUrl,
      facebookUrl: data.facebookUrl,
      nickName: data.nickName,
      seoUrl: data.seoUrl
    };
    this.reroute.unsubscribe();

    // SEO metadata
    this.title.setTitle(`Flow State ${this.athlete.name}'s Detail Page`);
    this.meta.addTag({name: 'description', content: `Flow State ${this.athlete.name}'s story about his origin`});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: `https://www.flowstate.com/athletes/${this.athlete.id}`});
    this.meta.addTag({name: 'twitter:title', content: `Flow State ${this.athlete.name}\'s Detail Page`});
    this.meta.addTag({name: 'twitter:description', content: `Flow State ${this.athlete.name}\'s story about his origin`});
    this.meta.addTag({name: 'twitter:text:description', content: `Flow State ${this.athlete.name}\'s story about his origin`});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: `https://www.flowstate.com/athletes/${this.athlete.id}`});
    this.meta.addTag({name: 'og:title', content: `Flow State ${this.athlete.name}\'s Detail Page`});
    this.meta.addTag({name: 'og:description', content: `Flow State ${this.athlete.name}\'s story about his origin`});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }
}
