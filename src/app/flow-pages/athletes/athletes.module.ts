import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AthletesComponent } from './athletes.component';
import { AthletesCardsComponent } from './athletes-cards/athletes-cards.component';
import { AthletesDetailComponent } from './athletes-detail/athletes-detail.component';

import { AthletesRoutingModule } from './athletes-routing.module';


@NgModule({
  declarations: [
    AthletesComponent,
    AthletesCardsComponent,
    AthletesDetailComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    AthletesRoutingModule
  ]
})
export class AthletesModule { }
