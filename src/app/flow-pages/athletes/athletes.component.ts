import { Component, OnInit, NgZone } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-athletes',
  templateUrl: './athletes.component.html',
  styleUrls: ['./athletes.component.sass']
})
export class AthletesComponent implements OnInit {

  public reroute;

  constructor(
    private zone: NgZone,
    private router: Router,
    private title: Title,
    private meta: Meta
  ) {
    this.reroute = router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.zone.run(() => {
          this.router.navigate(['/athletes']);
        });
      }
    });
  }

  ngOnInit() {

    // SEO metadata
    this.title.setTitle('Flow State Team Page');
    this.meta.addTag({name: 'description', content: 'The flow state\'s team page. Meet our team.'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/athletes'});
    this.meta.addTag({name: 'twitter:title', content: 'Flow State Team Page'});
    this.meta.addTag({name: 'twitter:description', content: 'The flow state\'s team page. Meet our team.'});
    this.meta.addTag({name: 'twitter:text:description', content: 'The flow state\'s team page. Meet our team.'});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/athletes'});
    this.meta.addTag({name: 'og:title', content: 'Flow State Team Page'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s team page. Meet our team.'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    this.reroute.unsubscribe();
  }

}
