import { Component, OnInit, Inject } from '@angular/core';

import { WINDOW } from '@ng-toolkit/universal';

import { AthleteService } from '../../../shared/services/athlete-service';

@Component({
  selector: 'app-athletes-cards',
  templateUrl: './athletes-cards.component.html',
  styleUrls: ['./athletes-cards.component.sass']
})
export class AthletesCardsComponent implements OnInit {

  public athletes;
  public changeText = false;

  constructor(
    @Inject(WINDOW) private window: Window,
    private athleteService: AthleteService
  ) {
  }

  ngOnInit() {
    this.athletes = this.athleteService.getAthletes();
  }

  goToInstagram(athleteUrl) {
    this.window.open(athleteUrl, '_blank');
  }

  goToFacebook(athleteUrl) {
    this.window.open(athleteUrl, '_blank');
  }

}
