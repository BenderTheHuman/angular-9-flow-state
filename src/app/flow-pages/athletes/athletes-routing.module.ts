import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AthletesComponent } from './athletes.component';
import { AthletesDetailComponent } from './athletes-detail/athletes-detail.component';

const routes: Routes = [
  {
    path: '',
    component: AthletesComponent,
    data: {animation: 'athletes'}
  },
  { path: ':id',
    component: AthletesDetailComponent,
    data: {animation: 'athletesDetails'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AthletesRoutingModule { }
