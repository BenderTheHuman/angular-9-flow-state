import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PricePageComponent } from './price-page.component';

import { PricePageRoutingModule } from './price-page-routing.module';


@NgModule({
  declarations: [
    PricePageComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    PricePageRoutingModule
  ]
})
export class PricePageModule { }
