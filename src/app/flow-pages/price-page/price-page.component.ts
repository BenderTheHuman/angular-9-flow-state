import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

import { SkyServiceService } from '../../shared/services/skydive-services.service';

@Component({
  selector: 'app-price-page',
  templateUrl: './price-page.component.html',
  styleUrls: ['./price-page.component.sass']
})
export class PricePageComponent implements OnInit {

  public skyProducts;

  constructor(
    private title: Title,
    private meta: Meta,
    private skyServices: SkyServiceService
  ) {
    this.skyProducts = this.skyServices.getSkyServices();
   }

  ngOnInit() {

    // SEO metadata
    this.title.setTitle('Flow State Services Page');
    this.meta.addTag({name: 'description', content: 'The flow state\'s services page. How can you learn more?'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/services'});
    this.meta.addTag({name: 'og:title', content: 'Flow State Services Page'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s services page. How can you learn more?'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/services'});
    this.meta.addTag({name: 'twitter:title', content: 'Flow State Services Page'});
    this.meta.addTag({name: 'twitter:description', content: 'The flow state\'s services page. How can you learn more?'});
    this.meta.addTag({name: 'twitter:text:description', content: 'The flow state\'s services page. How can you learn more?'});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

}
