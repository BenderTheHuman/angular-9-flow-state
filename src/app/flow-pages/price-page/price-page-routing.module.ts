import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricePageComponent } from './price-page.component';

const routes: Routes = [
  {
    path: '',
    component: PricePageComponent,
    data: {animation: 'pricePage'}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PricePageRoutingModule { }
