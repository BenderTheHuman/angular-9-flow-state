import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { OurStoryComponent } from './our-story.component';

import { OurStoryRoutingModule } from './our-story-routing.module';


@NgModule({
  declarations: [
    OurStoryComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    OurStoryRoutingModule
  ]
})
export class OurStoryModule { }
