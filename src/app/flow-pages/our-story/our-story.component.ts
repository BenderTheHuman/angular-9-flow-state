import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-our-story',
  templateUrl: './our-story.component.html',
  styleUrls: ['./our-story.component.sass']
})
export class OurStoryComponent implements OnInit {

  public originStory =
  `     Flow State Fly has been in an evolutionary stage for most of it's existence. Creating a platform to share our life experience with everyone. Humans all have something they love to do. Reaching the state of flow. The mental state which a person is performing fully immersed in a feeling of energized focus, full involvement, and enjoyment of the process. We are heavily focused on skydiving training and development. Creating plans to help others build up freefly, angle, canopy/parachutes and basic freefall skills. This also expands to other skill sports. In the future we plan to continue to expand our flow state horizons.

        If you're interested in a deeper version of our upcoming. A longer origin story is below.

        Starting in a condo next to a dropzone/skydiving operation. Our Founder was learning how to pack parachutes at his first skydiving job. After moving across the country to Nevada to take this job to start a new lifestyle. Unfortunately it didn't last more than 3 months because there was no good location close by to fun jump.
        After looking around for another start in the skydiving sport he got into a small dropzone in the middle of nowhere Arizona south of Phoenix. Learning how to pack parachutes quickly and in extreme conditions. Absolutely loved the desert playground he was working in. Fun jumpers were scarce but existent. The memories created by a group of friends vibing together in the middle of the desert while skydiving. Unfortunately it had to end because the owner was not paying and was weeks out. Even if he wanted to stay.
        So he was moving to Rockmart, Georgia before he knew it. Arriving just in time for Spaceland Atlanta's free jump day. It was going to be the start of a beautiful relationship. A place with fun jumpers, students and friends interested in the same things. It's an understatement to describe it was a paradise. Allowing skydivers to be able to have fun. Surviving and thriving. Building onto skills by constant play everyday.
        Through training and conversations he started to focus heavily in canopy/parachute piloting, angle and free fly skills. Getting into instructing skydiving students while continuing to grow. After leaving Georgia had a video setup and a tandem rating. On to Chicago he went.
        Arriving in Chicagoland to jump and see how awesome that place really is. He was only there because he was invited by some friends who wanted to open up new opportunities for him. Impressed with the facilities and functionality he hit the ground running. Skydiving everyday, green soft grass, fun jumps and good times. Unforgettable memories.
        Before he knew it the weather was starting to get cooler so the next move was to Texas close to Dallas. Where his game leveled up. Receiving his Accelerated Freefall Instructor/AFF-I rating. Getting the ability to share the sport with others who fell in love. Opening up the world of skydiving to others. Such a rewarding job.
        Did I mention how awesome the fun jumping community was while he was there. Letting him continue to grow and get challenged by the locals. Any moment he wasn't shooting video, doing AFF, taking a tandem or coaching. He would be fun jumping with anyone who would. Solo always looking to build!
        During the stay in Whitewright a friend relocated and went up to Indianapolis. That changed the plans because he was invited so he went. Accepting opportunities that allow him to hangout with friends and skydive. So on to Indianapolis he went.
        Arriving in a new place to skydive and make memories. Life is good. Always making new friend, celebrating and teaching new skills. Leveling up swooping day after day. In the time at indy he started progression on his 1080 turns under a velocity 79. Things are starting to get epic. Such a beautiful wing and fun turn!
        This is where flow state started building. Which were little to no plans at the time. The idea of growing something together. The season ending happened quickly and it was time to relocate again. Going back to Spaceland Dallas for a few months before heading to Florida.
        Spending around 2 years fun jumping and training canopy piloting. Before getting a job in Sebastian fun jumping, swooping and teaching. Continuing to build on the skills he has been working on since the beginning.

        Flow State is a platform to allow the connection of like minded individuals. People who would like to connect, plan and build things together. We offer coaching/instructing in angles, freefly and canopy/parachutes. Daily and per jump rates available. Use our experience in your favor and let's level up those skills.
        Thank you for spending the time to learn about the birth of Flow State. Spreading knowledge, and working towards larger community goals.
  `;

  constructor(
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {

    // SEO metadata
    this.title.setTitle('How Flow State Came Together: The origin story');
    this.meta.addTag({name: 'description', content: 'The flow state\'s origin story. How flow state\'s team came together'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/our-story'});
    this.meta.addTag({name: 'og:title', content: 'How Flow State Came Together: The Origin Story'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s origin story. How flow state\'s team came together'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/our-story'});
    this.meta.addTag({name: 'twitter:title', content: 'How Flow State Came Together: The Origin Story'});
    this.meta.addTag({name: 'twitter:description', content: 'The flow state\'s origin story. How flow state\'s team came together'});
    this.meta.addTag({name: 'twitter:text:description', content: 'The flow state\'s origin story. How flow state\'s team came together'});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

}
