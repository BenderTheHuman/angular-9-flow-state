import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { WINDOW } from '@ng-toolkit/universal';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.sass']
})
export class CardsComponent implements OnInit {

  public cardText = {
    story: 'Take a look into the Flow State Movement',
    team: 'Building a collection of some of the most rad/raddest individuals imaginable',
    calender: 'Our upcoming events calender, and let\'s go play!',
    media : 'View the world through our eyes!',
  };

  constructor(private router: Router, @Inject(WINDOW) private window: Window) {}

  ngOnInit() {}

  public goToPage(page, event) {
    switch (page) {
      case 'story':
        this.router.navigate(['/', 'our-story']);
        event.stopPropagation();
        this.window.scrollTo(0, 0);
        break;
      case 'team':
        this.router.navigate(['/', 'athletes']);
        event.stopPropagation();
        this.window.scrollTo(0, 0);
        break;
      case 'media':
        this.router.navigate(['/', 'media']);
        event.stopPropagation();
        this.window.scrollTo(0, 0);
        break;
      case 'calender':
        this.router.navigate(['/', 'calender']);
        event.stopPropagation();
        this.window.scrollTo(0, 0);
        break;
      default:
        return;
    }
  }
}
