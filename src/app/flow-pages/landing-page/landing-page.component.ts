import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.sass']
})
export class LandingPageComponent implements OnInit {

  constructor(
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {

    // SEO metadata
    this.title.setTitle('Flow State, A movement');
    this.meta.addTag({name: 'description', content: 'The flow state\'s website. Landing page to see what we are about.'});

    // Twitter metadata
    this.meta.addTag({name: 'twitter:card', content: 'summary'});
    this.meta.addTag({name: 'twitter:site', content: 'https://www.flowstate.com/home'});
    this.meta.addTag({name: 'twitter:title', content: 'Flow State Home Page'});
    this.meta.addTag({name: 'twitter:description', content: 'The flow state\'s website. Landing page to see what we are about.'});
    this.meta.addTag({name: 'twitter:text:description', content: 'The flow state\'s website. Landing page to see what we are about.'});
    this.meta.addTag({name: 'twitter:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});

    // facebook metadata
    this.meta.addTag({name: 'og:url', content: 'https://www.flowstate.com/home'});
    this.meta.addTag({name: 'og:title', content: 'Flow State Home Page'});
    this.meta.addTag({name: 'og:description', content: 'The flow state\'s website. Landing page to see what we are about.'});
    this.meta.addTag({name: 'og:image', content: 'https://www.flowstatefly.com/assets/images/stateOfFlow3dTan.png'});
  }

}
