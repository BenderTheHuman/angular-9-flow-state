import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AthleteService } from '../shared/services/athlete-service';
import { SkyServiceService } from '../shared/services/skydive-services.service';

import { AthletesModule } from './athletes/athletes.module';
import { PricePageModule } from './price-page/price-page.module';
import { OurStoryModule } from './our-story/our-story.module';
import { MediaModule } from './media/media.module';
import { LandingPageModule } from './landing-page/landing-page.module';
import { ConnectModule } from './connect/connect.module';
import { CalenderModule } from './calender/calender.module';

import { FlowAppRoutingModule } from './flow-app-routing.module';


@NgModule({
  declarations: [
  ],
  providers: [
    AthleteService,
    SkyServiceService
  ],
  imports: [
    CommonModule,
    LandingPageModule,
    PricePageModule,
    OurStoryModule,
    AthletesModule,
    MediaModule,
    ConnectModule,
    CalenderModule,
    FlowAppRoutingModule
  ]
})
export class FlowAppModule { }
