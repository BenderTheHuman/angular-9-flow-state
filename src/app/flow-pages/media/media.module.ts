import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MediaComponent } from './media.component';

import { MediaRoutingModule } from './media-routing.module';


@NgModule({
  declarations: [
    MediaComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    MediaRoutingModule
  ]
})
export class MediaModule { }

