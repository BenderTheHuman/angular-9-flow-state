import { Component, OnInit, Inject } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { WINDOW } from '@ng-toolkit/universal';

import { Router, NavigationEnd, ActivatedRoute, RouterOutlet } from '@angular/router';

import { filter, map } from 'rxjs/operators';

import { transAnimationLeft, transAnimationRight, transAnimationUp } from './shared/animations/route-animations';
import { trigger, transition, useAnimation } from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [
    trigger('routeAnimations', [
      transition('* => athletes', [
        useAnimation(transAnimationLeft)
      ]),
      transition('* => athletesDetails', [
        useAnimation(transAnimationUp)
      ]),
      transition('* => calender', [
        useAnimation(transAnimationRight)
      ]),
      transition('* => connect', [
        useAnimation(transAnimationRight)
      ]),
      transition('* => landingPage', [
        useAnimation(transAnimationLeft)
      ]),
      transition('* => pricePage', [
        useAnimation(transAnimationUp)
      ]),
      transition('* => media', [
        useAnimation(transAnimationRight)
      ]),
      transition('* => ourStory', [
        useAnimation(transAnimationLeft)
      ]),
      transition('* => flowStore', [
        useAnimation(transAnimationUp)
      ]),
  ])
  ]
})
export class AppComponent implements OnInit {
  title = 'FlowStateFly';
  public previousUrl;
  public updateForServiceWorker = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    @Inject(WINDOW) private window: Window,
    private swUpdate: SwUpdate) {

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute)
      ).toPromise().then(x => console.log(x));
  }

  ngOnInit() {

    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        this.updateForServiceWorker = true;
      });
  }
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  closeAlert() {
    this.updateForServiceWorker = false;
  }

  updateSW() {
    this.updateForServiceWorker = false;
    this.window.location.reload();
  }
}
