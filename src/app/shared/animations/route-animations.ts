import {
    trigger,
    transition,
    style,
    query,
    group,
    animateChild,
    animate,
    keyframes,
    animation
} from '@angular/animations';

export const transAnimationRight = animation([

    style({ position: 'relative' }),
    query(':enter, :leave', [
        style({
            position: 'absolute',
            left: 0,
            width: '100%'
        }),
    ], { optional: true }),
    group([
        query(':enter', [
          animate('1000ms ease', keyframes([
            style({ transform: 'translateX(-100%)', offset: 0 }),
            style({ transform: 'translateX(0%)', offset: 1 }),
          ])),
        ], { optional: true }),
        query(':leave', [
          animate('500ms ease', keyframes([
            style({ transform: 'translateX(0)', offset: 0 }),
            style({ transform: 'translateX(100%)', offset: 1 }),
          ])),
        ], { optional: true })
      ]),
]);

export const transAnimationLeft = animation([

    style({ position: 'relative' }),
    query(':enter, :leave', [
        style({
            position: 'absolute',
            left: 0,
            width: '100%'
        }),
    ], { optional: true }),
    group([
        query(':enter', [
          animate('1000ms ease', keyframes([
            style({ transform: 'translateX(100%)', offset: 0 }),
            style({ transform: 'translateX(0%)', offset: 1 }),
          ])),
        ], { optional: true }),
        query(':leave', [
          animate('500ms ease', keyframes([
            style({ transform: 'translateX(0)', offset: 0 }),
            style({ transform: 'translateX(-100%)', offset: 1 }),
          ])),
        ], { optional: true })
      ]),
]);

export const transAnimationUp = animation([

    style({ position: 'relative' }),
    query(':enter, :leave', [
        style({
            position: 'absolute',
            left: 0,
            width: '100%'
        }),
    ], { optional: true }),
    group([
        query(':enter', [
          animate('1000ms ease', keyframes([
            style({ transform: 'translateY(100%)', offset: 0 }),
            style({ transform: 'translateY(0%)', offset: 1 }),
          ])),
        ], { optional: true }),
        query(':leave', [
          animate('500ms ease', keyframes([
            style({ transform: 'translateY(0)', offset: 0 }),
            style({ transform: 'translateY(-200%)', offset: 1 }),
          ])),
        ], { optional: true })
      ]),
]);
