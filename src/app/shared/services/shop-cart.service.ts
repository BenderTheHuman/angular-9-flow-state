import { Injectable } from '@angular/core';
import { ProductsService } from './products.service';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShopCartService {

  private items = [];
  private total = 0;
  private newItemAddedToCart = new Subject<any>();

  constructor(
    private productService: ProductsService
  ) { }

  editingCart(addingtask: boolean) {
    this.newItemAddedToCart.next(addingtask);
  }

  didCartQuantityIncrease(): Observable<any> {
    return this.newItemAddedToCart.asObservable();
  }

  addToCart(id, quantity) {
    if (id) {
      const prod = this.productService.find(id);
      const item = {
        product: prod,
        quantity
      };
      if (localStorage.getItem('flow-state-cart') === null) {
        const cart: any = [];
        cart.push(JSON.stringify(item));
        localStorage.setItem('flow-state-cart', JSON.stringify(cart));
      } else {
        const cart = JSON.parse(localStorage.getItem('flow-state-cart'));
        let index = -1;
        for (let i = 0; i < cart.length; i++) {
          const ite = JSON.parse(cart[i]);
          if (ite.product.id === id) {
            index = i;
            break;
          }
        }
        if (index === -1) {
          cart.push(JSON.stringify(item));
          localStorage.setItem('flow-state-cart', JSON.stringify(cart));
        } else {
          const iteM = JSON.parse(cart[index]);
          iteM.quantity += quantity;
          cart[index] = JSON.stringify(iteM);
          localStorage.setItem('flow-state-cart', JSON.stringify(cart));
        }
      }
      this.editingCart(true);
    } else {
      console.log('Product Id doesnt exist anymore');
    }
  }

  loadCart() {
    this.total = 0;
    this.items = [];
    const cart = JSON.parse(localStorage.getItem('flow-state-cart'));
    if (cart && cart !== null) {
      for (let i = 0; i < cart.length; i++) {
        const item = JSON.parse(cart[i]);
        this.items.push({
          product: item.product,
          quantity: item.quantity,
          sku: item.product.sku
        });
        this.total += item.product.price * item.quantity;
      }
      this.items.push(this.total);
      return this.items;
    }
    return null;
  }

  loadSkus() {
    this.items = [];
    const cart = JSON.parse(localStorage.getItem('flow-state-cart'));
    if (cart && cart !== null) {
      for (let i = 0; i < cart.length; i++) {
        const item = JSON.parse(cart[i]);
        this.items.push({
          sku: item.product.sku,
          quantity: item.quantity
        });
      }
      return this.items;
    }
    return null;
  }

  remove(id: string) {
    const cart = JSON.parse(localStorage.getItem('flow-state-cart'));
    const index = -1;
    for (let i = 0; i < cart.length; i++) {
      const item = JSON.parse(cart[i]);
      if (item.product.id === id) {
        cart.splice(i, 1);
        break;
      }
    }
    localStorage.setItem('flow-state-cart', JSON.stringify(cart));
    this.editingCart(true);
    this.loadCart();
  }

  editItem(id: string, quantity) {
    const cart = JSON.parse(localStorage.getItem('flow-state-cart'));
    const index = -1;
    for (let i = 0; i < cart.length; i++) {
      const item = JSON.parse(cart[i]);
      if (item.product.id === id) {
        item.quantity = quantity;
        cart.splice(i, 1, JSON.stringify(item));
        break;
      }
    }
    localStorage.setItem('flow-state-cart', JSON.stringify(cart));
    this.editingCart(true);
    this.loadCart();
  }

  cartQuantity() {
    const cart = JSON.parse(localStorage.getItem('flow-state-cart'));
    let total = 0;
    if (cart !== null) {
      for (let i = 0; i < cart.length; i++) {
        const item = JSON.parse(cart[i]);
        total += +item.quantity;
      }
      return total;
    }
    return null;
  }
}
