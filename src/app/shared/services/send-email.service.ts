import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Email } from '../_classes/email';

@Injectable({
  providedIn: 'root'
})
export class SendEmailService {
  private apiURL: string = 'https://us-central1-flowstate-685cf.cloudfunctions.net/emailMessage';

  constructor(private httpClient: HttpClient) { }

  public sendEmail(data: Email) {
    const { name, email, phone, message } = data;
    return this.httpClient.post(`${this.apiURL}`, { name, email, phone, message });
  }
}
