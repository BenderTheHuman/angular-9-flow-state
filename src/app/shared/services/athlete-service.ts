import { Injectable } from '@angular/core';

import { athletesList } from '../athletes/athletes';

@Injectable({
  providedIn: 'root'
})
export class AthleteService {

  public athletes = athletesList;

  constructor() { }

  public getAthlete(athleteId) {
    return this.athletes.filter(x => x.id === athleteId);
  }

  public getAthletes() {
    return [ ...this.athletes ];
  }
}
