import { TestBed } from '@angular/core/testing';

import { ShopCart.ServiceService } from './shop-cart.service.service';

describe('ShopCart.ServiceService', () => {
  let service: ShopCart.ServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShopCart.ServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
