import { Injectable } from '@angular/core';

import * as data from '../../../assets/data/products.json';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public products: any = (data as any).default;

  constructor() { }

  public getProducts() {
    return [ ...this.products ];
  }

  public getAccessories() {
    return this.products[0].accessories;
  }

  public getShortSleeves() {
    return this.products[0].shortSleeves;
  }

  public getJerseys() {
    return this.products[0].jerseys;
  }

  find(id) {
    let items;
    // concat the array to make one array to search for id
    items = this.getJerseys().concat(this.getShortSleeves(), this.getAccessories());
    const foundItem = items.find(x => x.id === id);
    return foundItem;
  }
}
