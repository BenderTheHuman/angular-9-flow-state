import { Injectable } from '@angular/core';

import { skyProducts } from '../skydive-services/skydive-services';

@Injectable({
  providedIn: 'root'
})
export class SkyServiceService {

  public products = skyProducts;

  constructor() { }

  public getSkyService(Id) {
    return this.products.filter(x => x.id === Id);
  }

  public getSkyServices() {
    return [ ...this.products ];
  }
}
