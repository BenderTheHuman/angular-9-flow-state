export class Athlete {
  name: string;
  id: number;
  introduction: string;
  imageUrl: string;
  seoUrl: string;
  instagramUrl: string;
  facebookUrl: string;
  nickName: string;
  excerpt: string;
}
