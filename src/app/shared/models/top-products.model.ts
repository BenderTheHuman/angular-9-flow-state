export class TopProduct {
    id: string;
    name: string;
    desc: string;
    link: string;
}
