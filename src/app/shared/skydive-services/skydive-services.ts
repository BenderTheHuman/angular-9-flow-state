export const skyProducts = [
  {
    id: 1,
    imageUrl: '../assets/images/alex-angle.jpg',
    service: 'Organizing',
    types: [
      'events',
      'camps',
      'dropzones'
    ],
    description: 'Hearding cats (skydivers) into challenging and complex skydives testing skills. With the main goal of having fun.'
  },
  {
    id: 2,
    imageUrl: '../assets/images/tunnellaylout.png',
    service: 'Tunnel Coaching',
    types: [
      'bellyfly',
      'backfly'
    ],
    description: 'Specialized coaching to work on the finer movements. Using the wind tunnel to our advantage.'
  },
  {
    id: 3,
    imageUrl: '../assets/images/amirMobile.jpg',
    service: 'Skydive Coaching',
    types: [
      'approaching a formation',
      'exits (the hill)',
      'angles feet first',
      'angles head first',
      'bellyfly',
      'backfly',
      'vertical feet down',
      'vertical head down'
    ],
    description: 'Specialized coaching to work on skydiving specific skills.'
  },
  {
    id: 4,
    imageUrl: '../assets/images/swoop-dark.jpg',
    service: 'Canopy Coaching',
    types: [
      'customized canopy coaching dependant on progression',
      'become a safer canopy pilot',
      'swoop theory'
    ],
    description: 'Specialized coaching to work on parachute specific skills.'
  },
  {
    id: 5,
    imageUrl: '../assets/images/belly-round.jpg',
    service: 'Mini Camps',
    types: [
      'skydiving and tunnel infused mini camps',
      'simple diveflows',
      'complex dynamic dive flows',
      '',
      '',
      ''
    ],
    description: 'Mini camps to bring together small groups of skydivers (4) and work towards building skills.'
  }
];
