export const athletesList = [
  {
    name: 'Justin Bender',
    id: 1,
    introduction: `Born and raised in South Jersey.
                  A lover for anything extreme sports.
                  Viewing skydiving as a flow art,
                  he has spent the last 8 years travelling while training.
                  Learning more freefall, tunnel and canopy skills along the way.
                  When we asked him about his passion for coaching, he said,
                  "it's just incredible the things that we pull off!
                  I love sharing moments with others, while breaking personal boundries'
                  That look on someones face when something clicks.
                  Is the thing that makes it the most worth it, that smile you can't get rid of!"
                  Flow State is excited to be working with Justin!
                  Follow him with his social media links on the team page.`,
    imageUrl: '../../assets/images/standing.jpg',
    seoUrl: '/assets/images/standing.jpg',
    instagramUrl: 'https://www.instagram.com/justin.bender.experience/',
    facebookUrl: 'https://www.facebook.com/jrkbender',
    nickName: 'Bender',
    excerpt: 'Dream Big,  Spread Love,  Inspire Others!'
  },
  {
    name: 'Ben Cook',
    id: 2,
    introduction: `Ben is from North Carolina.
                  Competed heavily in sports as a child to being a young adult.
                  Focusing heavily on baseball and killin' it at the game.
                  From a young age Ben had drive travelling around the Globe exploring places like India,
                  throughout Europe and a few other countries.
                  Adventure is a lifestyle and it chose Ben, but I think Ben chose it too!
                  We couldn't be luckier to have Ben on the team because of the smiles he brings where he goes.
                  Follow him with his social media links on the team page.`,
    imageUrl: '../../assets/images/ben-stuff2.jpg',
    seoUrl: '/assets/images/ben-stuff2.jpg',
    instagramUrl: 'https://www.instagram.com/bcookie3084/',
    facebookUrl: 'https://www.facebook.com/bcookie3084',
    nickName: 'Tha Cookie Monsta',
    excerpt: `If you don't have cookies in the cookie jar, you can't eat cookies.`
  }
];
