interface Scripts {
  name: string;
  src: string;
  id: string;
}

export const ScriptStore: Scripts[] = [
  {
    name: 'shopify-buy-button',
    src: '../../assets/js/shopify-buy-button.js',
    id: 'shopify-script-id'
  },
  {
    name: 'shopify-init',
    src: '../../assets/js/shopify-buy-button.js',
    id: 'shopify-script-init-id'
  }
];
