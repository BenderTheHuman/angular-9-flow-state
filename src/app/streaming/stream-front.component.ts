import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';

import { Subscription } from 'rxjs';
import { WINDOW } from '@ng-toolkit/universal';

declare const flvjs: any;

@Component({
  selector: 'app-stream-front',
  templateUrl: './stream-front.component.html',
  styleUrls: ['./stream-front.component.sass']
})
export class StreamFrontComponent implements OnInit {

  public reroute;
  sub: Subscription;
  streamView = true;

  constructor(
    @Inject(WINDOW) private window: Window,
    @Inject(DOCUMENT) private doc: any,
    private title: Title,
    private meta: Meta
  ) {
  }


  ngOnInit() {
    this.setupFlv();
    // SEO metadata
    this.title.setTitle('Flow State Streaming');
    this.meta.addTag({ name: 'description', content: 'The flow state\'s streaming online games.' });

    // Twitter metadata
    this.meta.addTag({ name: 'twitter:card', content: 'summary' });
    this.meta.addTag({ name: 'twitter:site', content: 'https://www.flowstate.com/streaming' });
    this.meta.addTag({ name: 'twitter:title', content: 'Flow State Stream Games' });
    this.meta.addTag({ name: 'twitter:description', content: 'The flow state\'s streaming online games.' });
    this.meta.addTag({ name: 'twitter:text:description', content: 'The flow state\'s streaming online games.' });
    this.meta.addTag({ name: 'twitter:image', content: 'https://www.flowstate.com/assets/images/logoCircles.png' });
  }

  setupFlv() {
    if (flvjs.isSupported()) {
      const videoElement = document.getElementById('videoElement');
      const flvPlayer = flvjs.createPlayer({
          type: 'flv',
          url: 'https://192.168.7.234:8000/live/78473889887.flv'
      });
      flvPlayer.attachMediaElement(videoElement);
      flvPlayer.load();
      flvPlayer.play();
    }
  }
}
