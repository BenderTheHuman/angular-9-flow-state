import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { StreamRoutingModule } from './streaming-routing.module';
import { StreamFrontComponent } from './stream-front.component';

@NgModule({
  declarations: [
    StreamFrontComponent
  ],
  providers: [
  ],
  imports: [
    CommonModule,
    StreamRoutingModule
  ]
})
export class StreamModule { }
