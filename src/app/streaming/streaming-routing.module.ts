import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StreamFrontComponent } from './stream-front.component';


const routes: Routes = [
  {
    path: '',
    component: StreamFrontComponent
  }
  // {
  //   path: '**',
  //   conponent : PageNotFoundComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class StreamRoutingModule { }
