import { Component, OnInit, Inject } from '@angular/core';
// import { DOCUMENT } from '@angular/common';

import { WINDOW } from '@ng-toolkit/universal';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  // public bottomPage = false;

  constructor(
    @Inject(WINDOW) private window: Window,
    // @Inject(DOCUMENT) private document: Document
  ) { }

  // @HostListener('window:scroll', [])
  //   onScroll(): void {
  //   if (
  //     (this.window.innerHeight + this.window.scrollY) >= this.document.body.offsetHeight
  //     ||
  //     (this.window.innerHeight + this.window.scrollY) >= this.document.documentElement.offsetHeight
  //   ) {
  //     this.bottomPage = true;
  //   }
  //   if (
  //     (this.window.innerHeight + this.window.scrollY) < this.document.body.offsetHeight
  //     ||
  //     (this.window.innerHeight + this.window.scrollY) < this.document.documentElement.offsetHeight
  //   ) {
  //     this.bottomPage = false;
  //   }
  // }

  ngOnInit() {
  }

  public goToInstagram() {
    this.window.open('https://www.instagram.com/flowstatefly/', '_blank');
  }

  topScrollAndCollapse() {
    this.window.scrollTo(0, 0);
  }

  goToFooter() {
    this.window.scrollTo(0, 9999);
  }

  // public goToFacebook() {
  //   this.window.open('', '_blank');
  // }

}
