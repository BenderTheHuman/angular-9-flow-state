import { Component, HostListener, OnInit, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { WINDOW } from '@ng-toolkit/universal';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.sass']
})
export class NavBarComponent implements OnInit {

  public isCollapsed = true;
  public firstIcon = true;
  loginStatus = false;
  public flowStore = false;

  constructor(
    @Inject(WINDOW) private window: Window,
    private route: Router,
    private location: Location
  ) {
    this.route.events.subscribe((val) => {

      if (this.location.path().includes('shop') === true) {
        this.flowStore = true;
      } else if (this.location.path().includes('stream') === true) {
        this.flowStore = true;
      } else {
        this.flowStore = false;
      }
    });
   }

  ngOnInit() {
  }

  @HostListener('window:resize')

  sizeChange() {
    if (this.window.innerWidth > 574) {

      this.isCollapsed = true;
    }
  }

  topScrollAndCollapse() {
    this.isCollapsed = true;
    this.window.scrollTo(0, 0);
  }

  changeIcon() {
    (this.firstIcon === true) ? this.firstIcon = false : this.firstIcon = true;
  }

}
